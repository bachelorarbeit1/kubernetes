# Kubernetes Usage Control and Provenance Tracking Dummy Deployment

This repository contains Kubernetes manifests which can be used to deploy the [usage control and provenance tracking dummy components](https://gitlab.com/bachelorarbeit1) into a Kubernetes cluster. 

To do this you certainly need a Kubernetes cluster and some knowledge of how Kubernetes is working and how you can actually deploy something.

Take a look at the Helm package called ucon-prov in this repository. Via Helm and configuring the values.yaml file, you can modify the deployment to your needs, according to a seperation of divisions/namespaces in your organisation.

## Using Helm
Assuming Helm is already installed on your machine, go to `helm-package/ucon-prov/` and edit the `values.yaml` file according to your setup. Go back to the `helm-package` directory and run:

`helm package ucon-prov`

a package called `ucon-prov-0.1.1.tgz` has been created. You can now run:

`helm install ucon-prov ucon-prov-0.1.1.tgz`

this command installs the Helm package in your Kubernetes cluster according to your configuration set in `values.yaml`.